﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Diary.Enum;
using Diary.Interface;

namespace Diary.Models
{
    /// <summary>
    /// Модель записи оценки
    /// </summary>
    public class Mark : IEntity<int>
    {
        public int Id { get; set; }

        [ForeignKey("Teacher")]
        public int? TeacherId { get; set; }

        [ForeignKey("Student")]
        public int? StudentId { get; set; }

        // Значение оценки от 2 до 5
        public int Result { get; set; }
        public WorkType? WorkType { get; set; }

        // Школьный предмет
        public Subject Subject { get; set; }

        // Комментарий учителя
        [StringLength(1024)]
        public string Comment { get; set; }

        // Дата работы, за которую ставится оценка
        public DateTime? WorkDate { get; set; }

        // Дата, в которую оценка внесена в базу
        public DateTime CreateDate { get; set; }

        public Teacher Teacher { get; set; }
        public Student Student { get; set; }
    }
}
