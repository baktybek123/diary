﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Diary.Interface;

namespace Diary.Models
{
    public abstract class User : IEntity<int>, IPerson
    {
        public int Id { get; set; }

        [Required]
        [StringLength(40)]
        public string Login { get; set; }

        [Required]
        [StringLength(100)]
        public string Password { get; set; }

        [Required]
        [StringLength(40)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(40)]
        public string SurName { get; set; }

        public User()
        {

        }

        public User(string login, string password, string firstName, string surName)
        {
            Login = login;
            Password = password;
            FirstName = firstName;
            SurName = surName;
        }

        public virtual void PrintInfo(bool isReadKey = false, bool isClear = false)
        {
            if (isClear)
            {
                Console.Clear();
            }
            Console.WriteLine($"----------------------------");
            Console.WriteLine($"ID: {Id}");
            Console.WriteLine($" ID: {nameof(User)}");
            Console.WriteLine($"Login: {Login}");
            Console.WriteLine($"Password: {Password}");
            Console.WriteLine($"First name: {FirstName}");
            Console.WriteLine($"Surname: {SurName}");
            if (isReadKey)
            {
                Console.WriteLine("Нажмите чтоб продолжить");
                Console.ReadKey();
            }
        }
    }
}
