﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Diary.Enum;
using Diary.Interface;

namespace Diary.Models
{
    /// <summary>
    /// Расписание на один день недели для одного класса
    /// </summary>
    public class DaySchedule : IEntity<int>
    {
        public int Id { get; set; }

        [ForeignKey("Group")]
        public int? GroupId { get; set; }

        // День недели
        public DayOfWeek DayOfWeek { get; set; }

        // Урок первый, урок второй и т.д.
        public Subject? LessonN1 { get; set; }
        public Subject? LessonN2 { get; set; }
        public Subject? LessonN3 { get; set; }
        public Subject? LessonN4 { get; set; }
        public Subject? LessonN5 { get; set; }
        public Subject? LessonN6 { get; set; }

        public virtual Group Group { get; set; }
    }
}
