﻿using Diary.Enum;
using Diary.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diary.Models
{
    public class Teacher : User
    {
        public virtual ICollection<Group> Groups { get; set; }
        public virtual ICollection<Mark> Marks { get; set; }
    }
}
