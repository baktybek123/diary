﻿using Diary.Enum;
using Diary.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diary.Models
{
    public class Student : User
    {
        [ForeignKey("Group")]
        public int? GroupId { get; set; }

        [ForeignKey("Parent")]
        public int? ParentId { get; set; }

        public virtual Group Group { get; set; }
        public virtual Parent Parent { get; set; }
        public virtual ICollection<Mark> Marks { get; set; }

        public override void PrintInfo( bool isReadKey = false,bool isClear = false)
        {
            // base.PrintInfo();
            if (isClear)
                Console.Clear();
            Console.WriteLine();
            Console.WriteLine($"----------------------------");
            Console.WriteLine($"Your ID: {Id}");
            Console.WriteLine($"Login: {Login}");
            Console.WriteLine($"Password: {Password}");
            Console.WriteLine($"Firsname: {FirstName}");
            Console.WriteLine($"SurName: {SurName}");           
            Console.WriteLine($"Group id: {GroupId}");
            //Console.WriteLine($"Group name: {Group.GroupName}");
           // Console.WriteLine($"Parent fullname: {Parent.FirstName} {Parent.SurName}");
            if (isReadKey)
            {
                Console.WriteLine("Нажмите чтоб продолжить");
                Console.ReadKey();
            }
        }
        public void ConclutionOfClassmates()
        {
            Console.WriteLine();
            Console.WriteLine($"----------------------------");
            Console.WriteLine($"ID: {Id}");
            Console.WriteLine($"Firsname: {FirstName}");
            Console.WriteLine($"SurName: {SurName}");
            Console.WriteLine($"Group id: {GroupId}");

        }
    }
}
