﻿using Diary.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diary.Models
{
    /// <summary>
    /// класс или группа
    /// </summary>
    public class Group : IEntity<int>
    {
        public int Id { get; set; }

        [ForeignKey("Teacher")]
        public int? TeacherId { get; set; }

        [Required]
        //[StringLength(4)]
        public string GroupName { get; set; }

        [Required]
        public DateTime CreateDate { get; set; }

        public Teacher Teacher { get; set; }
        public ICollection<Student> Students { get; set; }
        public ICollection<DaySchedule> Schedules { get; set; }
    }
}
