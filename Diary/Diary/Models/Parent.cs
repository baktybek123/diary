﻿using Diary.Enum;
using Diary.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diary.Models
{
    public class Parent : User
    {
        public virtual ICollection<Student> Students { get; set; }
    }
}
