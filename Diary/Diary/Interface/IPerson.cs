﻿using Diary.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diary.Interface
{
    public interface IPerson
    {
        string Login { get; set; }
        string Password { get; set; }
        string FirstName { get; set; }
        string SurName { get; set; }
    }
}
