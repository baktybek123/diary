﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diary
{
    /// <summary>
    /// Позволяет добавить опции
    /// </summary>
    public class Option
    {
        public string Name { get; }
        public Action Selected { get; }
        public Option(string name, Action selected)
        {
            Name = name;
            Selected = selected;
        }
    }
}
