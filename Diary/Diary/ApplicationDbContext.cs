﻿using Diary.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diary
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Admin> Admins { get; set; }
        public DbSet<Parent> Parents { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<Teacher> Teachers { get; set; }
        /// <summary>
        /// /////////////
        /// </summary>
        public DbSet<Group> Groups { get; set; }
        public DbSet<DaySchedule> Schedules { get; set; }
        public DbSet<Mark> Marks { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            string connectionString = new ConfigurationBuilder()
                                        .SetBasePath(@"C:\Users\melma\source\repos\diary\Diary\Diary\")
                                          .AddJsonFile("appsettings.json")
                                           .Build()
                                           .GetConnectionString("DefaultConnection");
            optionsBuilder.UseSqlServer(connectionString);
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<User>()
                .HasIndex(i => i.Login)
                .IsUnique();

            builder.Entity<DaySchedule>()
                .HasIndex(i => new { i.GroupId, i.DayOfWeek })
                .IsUnique();

            builder.Entity<Group>()
                .HasOne(g => g.Teacher)
                .WithMany(t => t.Groups)
                .OnDelete(DeleteBehavior.SetNull);

            builder.Entity<Student>()
                .HasOne(s => s.Group)
                .WithMany(g => g.Students)
                .OnDelete(DeleteBehavior.SetNull);

            builder.Entity<Student>()
                .HasOne(s => s.Parent)
                .WithMany(p => p.Students)
                .OnDelete(DeleteBehavior.SetNull);

            builder.Entity<DaySchedule>()
                .HasOne(s => s.Group)
                .WithMany(g => g.Schedules)
                .OnDelete(DeleteBehavior.SetNull);

            builder.Entity<Mark>()
                .HasOne(s => s.Teacher)
                .WithMany(t => t.Marks)
                .OnDelete(DeleteBehavior.SetNull);

            builder.Entity<Mark>()
                .HasOne(s => s.Student)
                .WithMany(t => t.Marks)
                .OnDelete(DeleteBehavior.SetNull);
        }
    }
}
