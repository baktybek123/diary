﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Diary.Models;

namespace Diary.StaticMethods
{
    /// <summary>
    /// Набор статичных методов, связанных с общей пользовательской деятельностью
    /// </summary>
    public class UserStatics
    {
        public static bool Authorize<T>()
        {
            if (typeof(T) == Program._currentUser.GetType())
                return true;
            return false;
        }

        public static void SelectStudent()
        {
            bool isOkay = false;
            int GroupId = 0;

            Group group;
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                if(!db.Groups.Any())
                {
                    Program.Warning("Групп нету");
                    Console.ReadKey();
                    return;
                }
               
                while(!isOkay)
                {
                    SelectGroup(false,false);
                    GroupId = Program.InPutWhithCheck("Введите Id группы");

                    if(!db.Groups.Any(x => x.Id == GroupId))
                    {
                        Program.Warning("Такой не существует");
                    }
                    else
                    {
                        isOkay = true;
                    }
                }
                group = db.Groups.FirstOrDefault(x => x.Id == GroupId);
            }
           
                StudentStatics.ConclusionOfClassmates(group.Id);                           
            Console.ReadKey();
        }

        public static void SeeStudentGrade()
        {
            bool isOkay = true;
            int StudentId = 0;
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                if(!db.Students.Any())
                {
                    Console.WriteLine("Учеников нету");
                    Console.ReadKey();
                    return;
                }
                while(isOkay)
                {
                    GetUsersByType<Student>(false,false);
                    StudentId = Program.InPutWhithCheck("Введите ID ученника");

                    if(!db.Students.Any(x => x.Id == StudentId))
                    {
                        Program.Warning("Такого не существует");
                    }
                    else
                    {
                        isOkay = false;
                    }
                }

            }
           
                StudentStatics.ShowGrades(StudentId);
            
           

        }

        public static void SeeGroupScheduel()
        {

            SelectGroup(true,false);
            Group group;
            int? GroupiD = Program.InPutWhithCheck("Введите Id группы");
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                group = db.Groups.FirstOrDefault(x => x.Id == GroupiD);
            }
            if (group != null)
            {
                StudentStatics.ConclusionLessonSchedule(group.Id, true);
            }
            else
            {
                Program.Warning("ТАкой группы нету");
                Console.WriteLine("Нажмите чтоб продолжить");
                Console.ReadKey();
            }
        }


        public static void GetUsersByType<TUser>(bool isClear = true,bool isReadKey = true) where TUser : User
        {
            if (isClear)
                Console.Clear();
            var users = new List<TUser>();
            using (var db = new ApplicationDbContext())
            {


                var set = db.Set<TUser>();

                users = set.Where(x => true).ToList();

            }
            if (users.Count > 0)
            {
                foreach (var user in users)
                {

                    user.PrintInfo();
                }
            }
            else
            {
                Program.Warning("Никого нету");
            }
            if(isReadKey)
            {
                Console.WriteLine("Нажмите чтоб продолжить");
                Console.ReadKey();
            }
            
        }


        public static void SelectGroup(bool isClear = true,bool IsReadKey = true)
        {
            if (isClear)
                Console.Clear();
            List<Group> groups = new List<Group>();
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                groups = db.Groups.Where(x => true).ToList();
            }

            if (groups.Count > 0)
            {
                foreach ( var group in groups)
                {
                    Console.WriteLine("-------------------------------");
                    Console.WriteLine($"Id: {group.Id}");
                    Console.WriteLine($"Название группы: {group.GroupName}");
                    Console.WriteLine($"ID учителя: {group.TeacherId}");
                    Console.WriteLine($"Дата создания: {group.CreateDate}");
                    Console.WriteLine("-------------------------------");

                }
            }
            else
            {
                Program.Warning("Группа пуста");
            }
            if(IsReadKey)
            {
                Console.WriteLine("Нажмите чтоб продолжить");
                Console.ReadKey();
            }
            
        }


    }
}
