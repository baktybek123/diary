﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Diary.Enum;
using Diary.Models;

namespace Diary.StaticMethods
{
    /// <summary>
    /// Набор статичных методов, связанных с учительской деятельностью
    /// </summary>
    public static class TeacherStatics
    {
        /// <summary>
        /// Добавляет запись оценки в базу данных на основе указанных параметров.
        /// </summary>
        /// <param name="teacherId"></param>
        /// <param name="studentId"></param>
        /// <param name="result"></param>
        /// <param name="subject"></param>
        /// <param name="workType"></param>
        /// <param name="workDate"></param>
        /// <param name="comment"></param>
        public static void AddMark(int? teacherId, int? studentId, int result, Subject subject,
            WorkType? workType = null, DateTime? workDate = null, string comment = null)
        {
            // Проверки параметров на валидность
            if (teacherId <= 0)
                throw new ArgumentOutOfRangeException("ID учителя должен быть больше нуля.");
            if (studentId <= 0)
                throw new ArgumentOutOfRangeException("ID студента должен быть больше нуля.");
            if (result < 2 || result > 5)
                throw new ArgumentOutOfRangeException("Оценка должна быть от 2 до 5 баллов.");
            if (workDate != null && workDate > DateTime.Today.AddDays(1))
                throw new ArgumentException("Дата оцениваемой работы не может быть в будущем.");

            // Создает новый объект оценки
            var mark = new Mark()
            {
                TeacherId = teacherId,
                StudentId = studentId,
                Result = result,
                WorkType = workType,
                Subject = subject,
                Comment = comment,
                WorkDate = workDate,
                CreateDate = DateTime.Now
            };

            // Сохраняет созданный объект оценки в базу
            using (var db = new ApplicationDbContext())
            {
                db.Marks.Add(mark);
                db.SaveChanges();
            }
        }

        /// <summary>
        /// Удаляет запись оценки из базы данных по указанному идентификатору
        /// </summary>
        /// <param name="markID"></param>
        public static void DeleteMarkByID(int markID)
        {
            // Проверка на валидность параметров
            if (markID <= 0)
                throw new ArgumentOutOfRangeException("ID записи должен быть больше нуля.");

            using (var db = new ApplicationDbContext())
            {
                // Проверка на наличие записи в базе
                var mark = db.Marks.FirstOrDefault(m => m.Id == markID);
                if (mark == null)
                    throw new ArgumentNullException("Запись оценки не найдена.");

                db.Marks.Remove(mark);
                db.SaveChanges();
            }
        }

        /// <summary>
        /// Поиск записи оценки в базе данных по указанному идентификатору
        /// </summary>
        /// <param name="markID"></param>
        /// <returns></returns>
        public static Mark GetMarkByID(int markID)
        {
            // Проверка на валидность параметров
            if (markID <= 0)
                throw new ArgumentOutOfRangeException("ID записи должен быть больше нуля.");

            Mark mark = null;

            using (var db = new ApplicationDbContext())
            {
                // Поиск записи в базе
                mark = db.Marks.FirstOrDefault(m => m.Id == markID);
            }

            // Возвращает объект записи либо null значение (если не найдено)
            return mark;
        }
    }
}
