﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Diary.Enum;
using Diary.Interface;
using Diary.Models;

namespace Diary.StaticMethods
{
    /// <summary>
    /// Набор статичных методов, связанных с админской деятельностью
    /// </summary>
    public class AdminStatics
    {
        public static void AddNewUser<TUser>(TUser user) where TUser : User
        {
            if (user is Student student)
            {
                //Проверка
                if (student.GroupId < 0 ||
                    student.ParentId < 0)
                    throw new ArgumentException();

                //Добавляем в базу данных
                using (var db = new ApplicationDbContext())
                {
                    var set = db.Students;
                    //if (set.Any(x => x.Login == user.Login))
                    //{
                    // set.Add(student);
                    db.Students.Add(student);
                    db.SaveChanges();
                    //}
                }
                return;

            }

            //Добавляем в базу данных
            using (var db = new ApplicationDbContext())
            {
                var set = db.Set<TUser>();
                //if (set.Any(x => x.Login == user.Login))
                //{
                set.Add(user);
                db.SaveChanges();
                // }

            }
            //Console.WriteLine("Все прошло успешно.Нажмите чтоб продолжить");
            //Console.ReadKey();
        }

        public static void AddNewGroup(int teacherId, string groupName)
        {
            if (groupName == null ||
                groupName?.Trim() == "")
                throw new ArgumentException();

            //var subjects = System.Enum.GetValues(typeof(Subject)).Cast<Subject>();
            Group newgroup = new Group()
            {
                GroupName = groupName,
                TeacherId = teacherId,
                CreateDate = DateTime.Now
            };
            using (ApplicationDbContext db = new ApplicationDbContext())
            {

                db.Groups.Add(newgroup);
                db.SaveChanges();
            }

            AddShedule(newgroup.Id);
        }

        public static void DeleteGroup(string groupName)
        {
            if (groupName.Trim() == "" || groupName.Trim() == null)
                throw new ArgumentException();

            using (var db = new ApplicationDbContext())
            {
                var group = db.Groups.FirstOrDefault(g => g.GroupName == groupName);
                if (group != null)
                {
                    db.Groups.Remove(group);
                    db.SaveChanges();
                }
            }
        }

        public static void DeleteById<T>(int id) where T : class, IEntity<int>
        {
            bool check = true;

            if (id <= 0)
                throw new ArgumentException();

            using (var db = new ApplicationDbContext())
            {
                var set = db.Set<T>();
                var entity = set.FirstOrDefault(u => u.Id == id);

                if (entity != null)
                {
                    if (entity is User user)
                    {
                        check = Program.AskConfirm($"Этот пользователь будет удален:\n{user.Login}\n{user.FirstName} {user.SurName}");
                    }
                    else if (entity is Group group)
                    {
                        check = Program.AskConfirm($"Эта группа будет удалена:\n{group.GroupName}");
                    }

                    if (check)
                    {
                        set.Remove(entity);
                        db.SaveChanges();
                        return;
                    }
                }
            }
        }

        public static void DeleteUser<T>(string login) where T : User
        {
            using (var db = new ApplicationDbContext())
            {
                var set = db.Set<T>();
                var user = set.FirstOrDefault(u => u.Login == login);
                if (user != null)
                {
                    set.Remove(user);
                    db.SaveChanges();
                }
            }
        }

        public static void AddShedule(int GroupID)
        {
            for (int i = 1; i <= 5; i++)
            {
                Console.Clear();
                Console.WriteLine("Осталось составить расписпание");
                for (int u = 1; u <= 8; u++)
                {
                    Console.WriteLine((Enum.Subject)u + "-" + u);
                }
                Enum.DayOfWeek day = (Enum.DayOfWeek)i;
                Console.WriteLine($"Составьте рассписание на {day}");

                //Console.WriteLine(day);
                Subject? sub1, sub2, sub3, sub4, sub5, sub6;
                sub1 = Program.LoopTryParse<Subject?>("Введите первый урок");
                sub2 = Program.LoopTryParse<Subject?>("Введите второй  урок");
                sub3 = Program.LoopTryParse<Subject?>("Введите третий урок");
                sub4 = Program.LoopTryParse<Subject?>("Введите четвертый урок");
                sub5 = Program.LoopTryParse<Subject?>("Введите пятый урок");
                sub6 = Program.LoopTryParse<Subject?>("Введите шестой урок");
                using (var db = new ApplicationDbContext())
                {

                    db.Schedules.Add(new DaySchedule
                    {
                        GroupId = GroupID,
                        DayOfWeek = day,
                        LessonN1 = (Subject)sub1,
                        LessonN2 = (Subject)sub2,
                        LessonN3 = (Subject)sub3,
                        LessonN4 = (Subject)sub4,
                        LessonN5 = (Subject)sub5,
                        LessonN6 = (Subject)sub6
                    });
                    db.SaveChanges();
                }
            }
        }
    }
}
