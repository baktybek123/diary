﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Diary.Enum;
using Diary.Interface;
using Diary.Models;

namespace Diary.StaticMethods
{
    /// <summary>
    /// Набор статичных методов, связанных с учебной деятельностью
    /// </summary>
    public class StudentStatics
    {
        public static void ConclusionOfClassmates(int? GroupId,bool isClear = false)
        {
            if (isClear)
                Console.Clear();
            List<Student> students;
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                students = db.Students.Where(x => x.GroupId == GroupId).ToList();
            }

            if (students.Count > 0)
            {
                foreach (var s in students)
                {
                    s.ConclutionOfClassmates();
                }
            }
            else
            {
                Console.WriteLine("Эта группа пуста");
            }
            Console.WriteLine("Нажмите чтоб продолжить");
            Console.ReadKey();
        }


        public static void ShowGrades(int StudentId)
        {
                Console.Clear();
                //Subject subject = (Subject)InPutWhithCheck("По какому уроку хочешь посмотреть");
                var grades = new List<Mark>();
                var MathG = new List<Mark>();
                var EnglG = new List<Mark>();
                var BioG = new List<Mark>();
                var GeoG = new List<Mark>();
                var InfoG = new List<Mark>();
                var HisG = new List<Mark>();
                var LitG = new List<Mark>();
                var KyrgG = new List<Mark>();
                using (var db = new ApplicationDbContext())
                {
                    MathG = db.Marks.Where(x => x.StudentId == StudentId && x.Subject == Subject.Математика).ToList();
                    EnglG = db.Marks.Where(x => x.StudentId == StudentId && x.Subject == Subject.Английский).ToList();
                    BioG = db.Marks.Where(x => x.StudentId == StudentId && x.Subject == Subject.Биология).ToList();
                    GeoG = db.Marks.Where(x => x.StudentId == StudentId && x.Subject == Subject.География).ToList();
                    InfoG = db.Marks.Where(x => x.StudentId == StudentId && x.Subject == Subject.Информатика).ToList();
                    HisG = db.Marks.Where(x => x.StudentId == StudentId && x.Subject == Subject.История).ToList();
                    LitG = db.Marks.Where(x => x.StudentId == StudentId && x.Subject == Subject.Литература).ToList();
                    KyrgG = db.Marks.Where(x => x.StudentId == StudentId && x.Subject == Subject.Кыргызский).ToList();
                    double c = 0;
                    double result = 0;
                    //List<double> 
                    Console.Write("Английский язык     ");
                    foreach (Mark i in EnglG)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.Write(i.Result + " ");
                        c += i.Result;
                    }
                    result = c / EnglG.Count;
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine("  " + Math.Round(result, 2));
                    Console.ForegroundColor = ConsoleColor.White;
                    c = 0;
                    Console.Write("Биология            ");
                    foreach (Mark i in BioG)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.Write(i.Result + " ");
                        c += i.Result;
                    }
                    result = c / BioG.Count;
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine("  " + Math.Round(result, 2));
                    Console.ForegroundColor = ConsoleColor.White;
                    c = 0;
                    Console.Write("География           ");
                    foreach (Mark i in GeoG)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.Write(i.Result + " ");
                        c += i.Result;
                    }
                    result = c / GeoG.Count;
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine("  " + Math.Round(result, 2));
                    Console.ForegroundColor = ConsoleColor.White;
                    c = 0;
                    Console.Write("Математика          ");
                    foreach (Mark i in MathG)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.Write(i.Result + " ");
                        c += i.Result;
                    }
                    result = c / MathG.Count;
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine("  " + Math.Round(result, 2));
                    Console.ForegroundColor = ConsoleColor.White;
                    c = 0;
                    Console.Write("Информатика         ");
                    foreach (Mark i in InfoG)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.Write(i.Result + " ");
                        c += i.Result;
                    }
                    result = c / InfoG.Count;
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine("  " + Math.Round(result, 2));
                    Console.ForegroundColor = ConsoleColor.White;
                    c = 0;
                    Console.Write("История             ");
                    foreach (Mark i in HisG)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.Write(i.Result + " ");
                        c += i.Result;
                    }
                    result = c / HisG.Count;
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine("  " + Math.Round(result, 2));
                    Console.ForegroundColor = ConsoleColor.White;
                    c = 0;
                    Console.Write("Литература          ");
                    foreach (Mark i in LitG)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.Write(i.Result + " ");
                        c += i.Result;
                    }
                    result = c / LitG.Count;
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine("  " + Math.Round(result, 2));
                    Console.ForegroundColor = ConsoleColor.White;
                    c = 0;
                    Console.Write("Кыргызски язык      ");
                    foreach (Mark i in KyrgG)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.Write(i.Result + " ");
                        c += i.Result;
                    }
                    result = c / KyrgG.Count;
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine("  " + Math.Round(result, 1));
                    Console.ForegroundColor = ConsoleColor.White;
                    c = 0;
                }

            Console.ReadKey();
            
        }

        /// <summary>
        /// Метод для вывода расписание уроков
        /// </summary>
        /// <param name="studentGroupId"></param>
        public static void ConclusionLessonSchedule(int? studentGroupId,bool isClear = false)
        {
           
            List<DaySchedule> schedules = new List<DaySchedule>();
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                schedules = db.Schedules.Where(x => x.GroupId == studentGroupId).ToList();
            }
            //if (isClear)
                Console.Clear();
            if (schedules.Count > 0)
            {
                foreach (var sc in schedules)
                {
                    Console.WriteLine();
                    Console.WriteLine("День недели: " + sc.DayOfWeek);
                    Console.WriteLine("-----------------------------------");
                    Console.WriteLine(sc.LessonN1);
                    Console.WriteLine(sc.LessonN2);
                    Console.WriteLine(sc.LessonN3);
                    Console.WriteLine(sc.LessonN4);
                    Console.WriteLine(sc.LessonN5);
                    Console.WriteLine(sc.LessonN6);
                }
            }
            else
            {
                Console.WriteLine("Нету расписания");                
            }
            Console.WriteLine("Все прошло успешно.Нажмите чтоб продолжить");
            Console.ReadKey();


        }



    }
}
