﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Diary.Interface;
using Diary.Models;

namespace Diary.StaticMethods
{
    /// <summary>
    /// Набор статичных методов, связанных с родительской деятельностью
    /// </summary>
    public class ParentStatics
    {
        public static void SeeParentKids(int IdParent)
        {
            List<Student> students = new List<Student>();
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                students = db.Students.Where(x => x.ParentId == IdParent).ToList();
            }
            if(students.Count > 0)
            {
                foreach(var student in students)
                {
                    student.PrintInfo();
                }
            }
            else
            {
                Console.WriteLine("У вас нет детей");
            }
            Console.WriteLine("Нажмите чтоб продолжить");
            Console.ReadKey();

        }

        public static void SignInWithYourChildAccount(int ParentId)
        {
            int IdStudent = Program.InPutWhithCheck("Введите Id ребенка");
            Student kids;

            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                kids = db.Students.FirstOrDefault(x => x.ParentId == ParentId && x.Id == IdStudent);
            }
            if(kids !=null)
            {
                Program.OptionMenu(Program.GetOptions(kids));
            }
            else
            {
                Program.Warning("У вас нету такого ребенка");
            }
            Console.WriteLine("Нажмите чтоб продолжить");
            Console.ReadKey();

        }
    }
}
