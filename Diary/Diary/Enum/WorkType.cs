﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Diary.Enum
{
    public enum WorkType
    {
        ClassWork = 1,
        HomeWork = 2,
        Test = 3,
        Exam = 4,
    }
}
