﻿using Diary.Enum;
using Diary.Interface;
using Diary.Models;
using Diary.StaticMethods;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace Diary
{
    public class Program
    {
        public static string defaultResponse = $"  [Up] Вверх [Down] Вниз ."
                                              + "\n  [Enter] выбрать опцию."
                                              + "\n  [Escape] вернуться назад.\n  ";
        static int index = 0;
        static ConsoleKeyInfo keyinfo;
        static DateTime currentTime;
        static bool IsOkay;

        public static User _currentUser = null;
        static List<Option> options = null;

        static List<Option> AddUserAndGroup = new List<Option>
        {
            new Option("Добавить группу", () => AddNewGroup()),
            new Option("Добавить админа", () => AddNewUser<Admin>()),
            new Option("Добавить учителя", () => AddNewUser<Teacher>()),
            new Option("Добавить родителя", () => AddNewUser<Parent>()),
            new Option("Добавить студента", () => AddNewUser<Student>()),
        };

        static List<Option> DeleteUserAndGroup = new List<Option>
        {
            new Option("Удалить группу", () => DeleteGroup()),
            new Option("Удалить пользователя", () => DeleteUser())
        };

        static void Main(string[] args)
        {
            Autorization();
        }

        static void AddNewGroup()
        {
            int teacherId;
            string groupName = " ";
            using (var db = new ApplicationDbContext())
            {
                do
                {
                    UserStatics.GetUsersByType<Teacher>(false, false);
                    teacherId = InPutWhithCheck("Введите ID учителя группы:");
                    if (teacherId > 0 && db.Teachers.Any(u => u.Id == teacherId))
                    {
                        break;
                    }
                    else
                    {
                        Warning("Такого учителя не сущечствует");
                    }
                } while (true);
            }


            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                IsOkay = false;
                while (!IsOkay)
                {
                    Console.WriteLine("Введите название группы:");
                    groupName = Console.ReadLine();
                    IsOkay = CheckWithNoSpace(groupName);
                    if (!IsOkay)
                        Warning("Нельзя с пробелами");
                    if (IsOkay && db.Groups.Any(x => x.GroupName == groupName))
                    {
                        IsOkay = false;
                        Warning("Название с такой группой уже есть");
                    }
                }

            }


            AdminStatics.AddNewGroup(teacherId, groupName);

            Console.WriteLine("Все прошло успешно.Нажмит на клавиатуру чтоб продолжить");
            Console.ReadKey();
        }

        /// <summary>
        /// Добавляет нового пользователя на основе введенных с консоли данных.
        /// </summary>
        /// <typeparam name="TUser"></typeparam>
        static void AddNewUser<TUser>()
            where TUser : User, new()
        {
            // Console.Clear();
            string login, password, firstName, surName;

            login = LoopTryParse<string>("Введите логин:", false, 4, 30);
            foreach (char c in login)
            {
                if (c == ' ')
                {
                    Warning("Без пробелов ЧУВАК");
                    AddNewUser<TUser>();
                }
            }
            User user;
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                user = db.Users.FirstOrDefault(x => x.Login == login);
            }
            if (user != null)
            {
                Warning("Ник занят");
                AddNewUser<TUser>();
            }
            password = LoopTryParse<string>("Введите пароль не менее 4:", false, 4);

            foreach (char c in password)
            {
                if (c == ' ')
                {
                    Warning("Без пробелов ЧУВАК");
                    AddNewUser<TUser>();
                }
            }

            firstName = LoopTryParse<string>("Введите имя:", false, 2, 30);
            surName = LoopTryParse<string>("Введите фамилию:", false, 2, 30);

            if (typeof(TUser) == typeof(Student))
            {
                int groupId;
                int? parentId;

                groupId = LoopTryParse<int>("Введите ID группы:");
                parentId = LoopTryParse<int?>("Введите ID родителя, либо ничего:", true);

                AdminStatics.AddNewUser(new Student()
                {
                    Login = login.Trim().ToLower(),
                    Password = password.Trim(),
                    FirstName = firstName.Trim(),
                    SurName = surName.Trim(),
                    GroupId = groupId,
                    ParentId = parentId
                });
                Console.WriteLine("Нажмите на клавиатуру чтоб продолжить");
                Console.ReadKey();
                return;
            }

            AdminStatics.AddNewUser(new TUser()
            {
                Login = login.Trim().ToLower(),
                Password = password.Trim(),
                FirstName = firstName.Trim(),
                SurName = surName
            });

            Console.WriteLine("Нажмите на клавиатуру чтоб продолжить");
            Console.ReadKey();
        }

        static void DeleteUser()
        {
            int id = 0;
            string login = null;
            do
            {
                Console.Clear();
                Console.WriteLine("Вы хотите удалить пользователя по id(1) или логину(2)?");
                switch (Console.ReadKey().Key)
                {
                    case ConsoleKey.D1:
                        id = LoopTryParse<int>("Введите ID удаляемого пользователя:");
                        break;
                    case ConsoleKey.D2:
                        login = LoopTryParse<string>("Введите логин удаляемого пользователя:");
                        break;
                    case ConsoleKey.Escape:
                        return;
                    default:
                        continue;
                }
                break;
            } while (true);

            if (login == null)
            {
                try
                {
                    AdminStatics.DeleteById<User>(id);
                    Console.WriteLine("id удален");
                }
                catch { }
            }
            else
            {
                try
                {
                    AdminStatics.DeleteUser<User>(login);
                    Console.WriteLine("login удален");
                }
                catch { }
            }
            Console.ReadKey();
        }

        static void DeleteGroup()
        {
            int groupId;
            using (var db = new ApplicationDbContext())
            {
                do
                {
                    groupId = LoopTryParse<int>("Введите id группы к удалению:");
                    if (db.Groups.Any(g => g.Id == groupId)) break;
                } while (true);
            }

            AdminStatics.DeleteById<Group>(groupId);
        }

        /// <summary>
        /// Добавляет новую запись оценки в базу на основе введенных с консоли данных.
        /// </summary>
        static void AddNewMark()
        {
            int teacherId, studentId = 0, result;
            Subject subject;
            WorkType? workType = null;
            DateTime? workDate = null;
            string comment = null;

            teacherId = 10 /*|| _currentUser.Id*/;

            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                if (!db.Students.Any())
                {
                    Warning("Студентов  нету");
                    Console.ReadKey();
                    return;
                }
                IsOkay = false;
                while (!IsOkay)
                {
                    UserStatics.GetUsersByType<Student>(false, false);
                    studentId = LoopTryParse<int>("Введите ID ученика:");
                    if (db.Students.Any(x => x.Id == studentId))
                    {
                        IsOkay = true;
                    }
                    else
                    {
                        Warning("Нетууу такого ученника");
                    }
                }


            }
            do
            {
                result = LoopTryParse<int>("Введите оценку (2-5):");
                if (result >= 2 && 5 >= result) break;
            } while (true);
            subject = LoopTryParse<Subject>("Введите номер предмета:\n" +
                                            "Английский (1)\n" +
                                            "Биология (2)\n" +
                                            "География (3)\n" +
                                            "Информатика (4)\n" +
                                            "История (5)\n" +
                                            "Кыргызский (6)");
            workType = LoopTryParse<WorkType?>("Введите тип задания, либо ничего:", true);
            workDate = LoopTryParse<DateTime?>("Введите дату в формате ГГГГ-ММ-дд, либо оставьте пустым:", true, 10, 10);
            comment = LoopTryParse<string>("Введите комментарий, либо оставьте пустым:", true);
            TeacherStatics.AddMark(teacherId, studentId, result, subject, workType, workDate, comment);
            Console.WriteLine("Все прошло успешно.Нажмите чтоб продолжить");
            Console.ReadKey();

        }

        /// <summary>
        /// Вызывает цикл, из которого можно выйти только успешно сконвертировав ввод с консоли.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="message"></param>
        /// <param name="minLength"></param>
        /// <param name="maxLength"></param>
        /// <returns></returns>
        public static T LoopTryParse<T>(string message = null, bool nullable = false, int minLength = 0, int maxLength = 0)
        {
            bool noWarning = true;
            do
            {
                //if (noWarning)
                //    Console.Clear();
                //else noWarning = true;

                Console.WriteLine(message);
                string input = Console.ReadLine();

                if (!nullable && (input == null || input.Trim() == ""))
                    continue;
                if (nullable && (input == null || input.Trim() == ""))
                    return default;
                if (minLength > 0 && minLength <= maxLength && input.Length <= minLength)
                {
                    Warning("Слишком коротко!");
                    noWarning = false;
                    continue;
                }
                if (maxLength > 0 && minLength <= maxLength && input.Length > maxLength)
                {
                    Warning("Слишком длинно!");
                    noWarning = false;
                    continue;
                }

                try
                {
                    T result = (T)TypeDescriptor.GetConverter(typeof(T)).ConvertFromString(input);
                    if (!typeof(T).IsEnum || System.Enum.IsDefined(typeof(T), result))
                    {
                        return result;
                    }
                    else
                    {
                        Warning("Неверные данные!");
                        noWarning = false;
                        continue;
                    }
                }
                catch
                {
                    Warning("Неверные данные!");
                    noWarning = false;
                    continue;
                }
            }
            while (true);
        }

        /// <summary>
        /// Метод нужен для выведения ошибки,которую пользователь мог допустить.Через параметр "message" мы и передаем текст ошибки
        /// </summary>
        /// <param name="message"></param>
        public static void Warning(string message)
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine($"Ошибка: {message}");
            //Console.WriteLine("Попробуйте еще раз.");
            Console.ResetColor();
        }

        /// <summary>
        /// Метод создан для передвижения по опциям
        /// </summary>
        /// <param name="ops"></param>
        public static void OptionMenu(List<Option> ops)
        {
            do
            {
                WriteMenu(ops);
                keyinfo = Console.ReadKey();
                if (keyinfo.Key == ConsoleKey.Escape)
                    return;
                if (keyinfo.Key == ConsoleKey.DownArrow)
                {
                    if (index + 1 < ops.Count)
                    {
                        index++;
                        WriteMenu(ops);
                    }
                }
                if (keyinfo.Key == ConsoleKey.UpArrow)
                {
                    if (index - 1 >= 0)
                    {
                        index--;
                        WriteMenu(ops);
                    }
                }
                if (keyinfo.Key == ConsoleKey.Enter)
                {
                    ops[index].Selected.Invoke();
                    index = 0;
                }

            }
            while (true);
        }

        /// <summary>
        /// Отвечает за то чтоб пользователь понял какую опцию он выбирает
        /// </summary>
        /// <param name="options"></param>
        static void WriteMenu(List<Option> options)
        {
            Console.Clear();
            Console.WriteLine(currentTime = DateTime.Now);
            Console.WriteLine($"Имя {_currentUser.FirstName}");
            Console.WriteLine($"Позиция {_currentUser.GetType().Name}");
            Console.WriteLine("-------------------------------------");
            foreach (Option option in options)
            {
                if (option == options[index])
                {
                    Console.BackgroundColor = ConsoleColor.DarkYellow;
                }
                else
                {
                    Console.Write(" ");
                }
                Console.WriteLine(option.Name);
                Console.ResetColor();
            }
            Console.WriteLine("---------------------------");
            Console.WriteLine(defaultResponse);
        }

        public static int InPutWhithCheck(string message)
        {
            int number = 0;
            bool IsOkay = false;
            while (!IsOkay)
            {
                Console.WriteLine(message);
                IsOkay = int.TryParse(Console.ReadLine(), out number);
                if (!IsOkay)
                {
                    Warning("Вы ввели не коректное число число");
                }
                if (IsOkay && number < 0)
                {
                    IsOkay = false;
                    Warning("Ты ввел отрицательное число");
                }
            }
            return number;
        }
        /// <summary>
        /// Возвращает список всех членов указанного Enum типа.
        /// </summary>
        /// <typeparam name="TEnum"></typeparam>
        /// <returns></returns>
        public static IEnumerable<TEnum> GetEnumList<TEnum>() where TEnum : System.Enum
        {
            return System.Enum.GetValues(typeof(TEnum)).Cast<TEnum>();
        }

        /// <summary>
        /// Выводит в консоль список всех членов указанного Enum типа.
        /// </summary>
        /// <typeparam name="TEnum"></typeparam>
        public static void WriteEnumList<TEnum>() where TEnum : System.Enum
        {
            foreach (var item in GetEnumList<TEnum>())
            {
                Console.WriteLine(item);
            }
        }


        public static List<Option> GetOptions(User user)
        {
            if (user is Admin admin)
            {
                return new List<Option>
                {
                    new Option("Посмотреть свои данные", () => admin.PrintInfo(true,true)),
                    new Option("Опции по добавлению ", () => AddEnitiy()),
                    new Option("Опции по удаления ", () => DeleteEnitiy()),
                    new Option("Посмотреть список класса",() => UserStatics.SelectStudent()),
                    new Option("Посмотреть оценки ученника",() => UserStatics.SeeStudentGrade()),
                    new Option("Посмотреть расписание класса",() => UserStatics.SeeGroupScheduel()),
                    new Option("Посмотреть список админов",() => UserStatics.GetUsersByType<Admin>()),
                    new Option("Посмотреть список учителей",() => UserStatics.GetUsersByType<Teacher>()),
                    new Option("Посмотреть список родителей",() => UserStatics.GetUsersByType<Parent>()),
                    new Option("Посмотреть список ученников",() => UserStatics.GetUsersByType<Student>()),
                    new Option("Посмотреть список групп",() => UserStatics.SelectGroup()),
                };
            }
            else if (user is Parent parent)
            {
                return new List<Option>
                {
                    new Option("Посмотреть свои данные", () => parent.PrintInfo(true,true)),
                    new Option("Посмотреть список своих детей",() => ParentStatics.SeeParentKids(parent.Id)),
                    new Option("Зайти через аккаунт ребенка",() => ParentStatics.SignInWithYourChildAccount(parent.Id)),
                    new Option("Посмотреть расписание класса",() => UserStatics.SeeGroupScheduel()),

                };
            }
            else if (user is Student student)
            {
                return new List<Option>
                {
                    new Option("Посмотреть свои данные", () => student.PrintInfo(true,true)),
                    new Option("Посмотреть своих одноклассников", () => StudentStatics.ConclusionOfClassmates(student.GroupId,true)),
                    new Option("Посмотреть оценки", () => StudentStatics.ShowGrades(student.Id)),
                    new Option("Посмотреть расписание",() => StudentStatics.ConclusionLessonSchedule(student.GroupId))

                };
            }
            else if (user is Teacher teacher)
            {
                return new List<Option>
                {
                    new Option("Посмотреть свои данные", () => teacher.PrintInfo(true,true)),
                    new Option("Поставить оценку",() => AddNewMark()),
                    new Option("Посмотреть оцуенки ученника",() => UserStatics.SeeStudentGrade()),
                    new Option("Посмотреть список класса",() => UserStatics.SelectStudent()),
                    new Option("Посмотреть расписание класса",() => UserStatics.SeeGroupScheduel()),
                };
            }
            throw new Exception("User Has No Type");
        }
        /// <summary>
        /// Ищет пользователя в базе по основному айди.
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public static User GetTypedUser(int id)
        {
            User result = null;
            using (var db = new ApplicationDbContext())
            {
                result = db.Students.FirstOrDefault(u => u.Id == id);
                if (result != null)
                    return result;

                result = db.Teachers.FirstOrDefault(u => u.Id == id);
                if (result != null)
                    return result;

                result = db.Parents.FirstOrDefault(u => u.Id == id);
                if (result != null)
                    return result;

                result = db.Admins.FirstOrDefault(u => u.Id == id);
                if (result != null)
                    return result;
            }
            return default;
        }

        static void Autorization(bool isClear = false)
        {
            if (isClear)
                Console.Clear();
            //_currentUser = null;
            string login = LoopTryParse<string>("введите логин");
            string password = LoopTryParse<string>("Введите пароль");
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                _currentUser = db.Users.FirstOrDefault(x => x.Login == login && x.Password == password);
            }
            if (_currentUser != null)
            {
                OptionMenu(GetOptions(_currentUser));
            }
            else
            {
                Warning("Мы вас не нашли");
                Autorization();
            }
            Autorization(true);

        }

        static void AddEnitiy()
        {
            OptionMenu(AddUserAndGroup);
        }

        static void DeleteEnitiy()
        {
            OptionMenu(DeleteUserAndGroup);
        }


        /// <summary>
        /// Проверка на отсутсвие пробелов
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        static bool CheckWithNoSpace(string text)
        {
            foreach (char c in text)
            {
                if (c == ' ')
                    return false;
            }
            return true;
        }

        public static bool AskConfirm(string question = null)
        {
            if (question != null && question.Trim() != "")
            {
                Console.WriteLine(question);
                Console.WriteLine();
            }

            Console.WriteLine("\nВы уверены? (Нажмите Enter для подтверждения)");
            if (Console.ReadKey().Key == ConsoleKey.Enter)
                return true;
            return false;
        }
    }
}
